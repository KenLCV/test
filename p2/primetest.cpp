/*
 * CSc103 Project 2: prime numbers.
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:
 *
 */

#include <iostream>
using std::cin;
using std::cout;
using std::endl;
#include <cmath>

int main()
{
  long input;
  int counter;
  int temp;

  counter = 0;
  while(cin >> input)
   {

     temp = input;

     while(counter < 3)
     {
       if(input == 1)
       {
        cout << "0 \n";
        break;
       }
       if(input%temp == 0)
       {
         counter++;
       }
       if(counter>2)
       {
         cout << "0 \n"; //NOT A PRIME
       }
       if(counter == 2 && temp == 1)
       {
         cout << "1 \n"; //IS PRIME
         break;
       }
      temp--;
     }
   counter = 0;
  }
    return 0;
}
