/*
 * CSc103 Project 3: Game of Life
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:
 *
 *
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours: 70
 */
#include<iostream>
#include <cstdio>
#include <stdlib.h> // for exit();
#include <getopt.h> // to parse long arguments.
#include <unistd.h> // sleep
using std::cout;
using std::cin;
#include <vector>
using std::vector;
#include <string>
using std::string;


static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Text-based version of Conway's game of life.\n\n"
"   --seed,-s     FILE     read start state from FILE.\n"
"   --world,-w    FILE     store current world in FILE.\n"
"   --fast-fw,-f  NUM      evolve system for NUM generations and quit.\n"
"   --help,-h              show this message and exit.\n";

size_t max_gen = 0; /* if > 0, fast forward to this generation. */
string wfilename =  "/home/csc103/csc103-projects/p3/grid.txt"; /* write state here */
FILE* fworld = 0; /* handle to file wfilename. */
string initfilename = "/home/csc103/csc103-projects/p3/starter.txt"; /* read initial state from here. */

// count neighbors of cell i,j on grid g:
size_t nbrCount(size_t i, size_t j, const vector<vector<bool> >& g);
void update(); // transform the old version of the world into the new one
int initFromFile(const string& fname); // read initial state from file
void mainLoop();
void dumpState(FILE* fn); // write the state to a file

char text[3] = ".O";
vector<vector<bool> > OldGrid;
vector<vector<bool> > NewGrid;
int row, col;
bool alive = true;
bool dead = false;

int initFromFile(const string& fname)
{
	FILE*f;
	if(fname != "-")
	{
		f = fopen(fname.c_str(), "rb"); //conversion to char
	}
	else
	{
		f = stdin;
	}
  if(!f)
  {
    cout << "Error, could not open file!";
    exit(1);
  }
  OldGrid.push_back(vector<bool>()); //add a new row;
  char c;

  while(fread(&c,1,1,f))
  {
    if(c == '\n')
    {
      row++;//found newline add new row
      OldGrid.push_back(vector<bool>());
    }
    else if(c == '.')
    {
			OldGrid[row].push_back(false);
    }
    else
    {
			OldGrid[row].push_back(true);
    }
  }
	col = OldGrid[0].size();

  fclose(f);
	NewGrid = OldGrid;
	return 0;
}

void dumpState(FILE* fn)
{
	// if (wfilename == "-") {set file handle to stdout...
	if(wfilename != "-")
	{
		fn = fopen(wfilename.c_str(), "wb");
	}
	else
	{
		fn = stdout;
	}

  char c = '.';
  for(int a = 0; a <row; a++)
  {
    for(int b = 0; b < col; b++)
    {
      if(NewGrid[a][b] == alive)
      {
        c = 'O';
      }
      else
			{
        c = '.';
      }
      fwrite(&c,1,1,fn);
    }
		if(a <= row)
		{
			c = '\n';
			fwrite(&c,1,1,fn);
		}
  }
	rewind(fn);
}

void update()
{
	OldGrid = NewGrid;
	int n;
	for(int i = 0; i <row;i++)
	{
		for(int j = 0; j <col; j++)
		{
			n = 0;
			//Check top left
			if(OldGrid[(i-1+row)%row][(j-1+col)%col])
			{
				n++;
			}

			//Check top
			if(OldGrid[(i-1+row)%row][j])
			{
				n++;
			}

			//Check Top right
			if(OldGrid[(i-1+row)%row][(j+1+col)%col])
			{
				n++;
			}

			//Check left
			if(OldGrid[i][(j-1+col)%col])
			{
				n++;
			}

			//Check right
			if(OldGrid[i][(j+1+col)%col])
			{
				n++;
			}

			//Check bottom left
			if(OldGrid[(i+1+row)%row][(j-1+col)%col])
			{
				n++;
			}

			//Check bottom
			if(OldGrid[(i+1+row)%row][j])
			{
				n++;
			}

			//Check bottom right
			if(OldGrid[(i+1+row)%row][(j+1+col)%col])
			{
				n++;
			}
			if((OldGrid[i][j] == dead) && n == 3)
		 {
			 NewGrid[i][j] = alive;
		 }

		 if(OldGrid[i][j] == alive)
		 {
				if(n > 3 || n < 2)
				{
					NewGrid[i][j] = dead;
				}
				if(n == 2 || n == 3)
				{
					NewGrid[i][j] = alive;
				}
			}
		}
	}
}

int main(int argc, char *argv[]) {
	// define long options
	static struct option long_opts[] = {
		{"seed",    required_argument, 0, 's'},
		{"world",   required_argument, 0, 'w'},
		{"fast-fw", required_argument, 0, 'f'},
		{"help",    no_argument,       0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "hs:w:f:", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case 's':
				initfilename = optarg;
				break;
			case 'w':
				wfilename = optarg;
				break;
			case 'f':
				max_gen = atoi(optarg);
				break;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}

		initFromFile(initfilename);
		mainLoop();
		return 0;
}
void mainLoop()
{
	size_t counter = 0;
	//Mode 2
		if(max_gen != 0)
		{
			while(counter < max_gen)
			{
				update();
				dumpState(fworld);
				counter++;
			}
		}
		//Mode 1
		else
		{
			while(true)
			{
				update();
				dumpState(fworld);
				sleep(1);
			}
		}
}